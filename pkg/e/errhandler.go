package e

import "net/http"

type ErrHandler struct {
	HttpCode int
	Error    error
}

func (e *ErrHandler) Err() bool {
	return e.Error != nil
}

func Success() *ErrHandler {
	return &ErrHandler{
		HttpCode: http.StatusOK,
		Error:    nil,
	}
}

func BadRequest(err error) *ErrHandler {
	return &ErrHandler{
		HttpCode: http.StatusBadRequest,
		Error:    err,
	}
}

func InternalServerError(err error) *ErrHandler {
	return &ErrHandler{
		HttpCode: http.StatusInternalServerError,
		Error:    err,
	}
}

func Unauthorized(err error) *ErrHandler {
	return &ErrHandler{
		HttpCode: http.StatusUnauthorized,
		Error:    err,
	}
}
