package ginhandlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Butanal/personal-lib/pkg/e"
)

func JSONResponse(c *gin.Context, data interface{}, errHandler *e.ErrHandler) {
	var err string

	if errHandler.Error != nil {
		err = errHandler.Error.Error()
	}

	c.JSON(errHandler.HttpCode, gin.H{
		"success": errHandler.HttpCode <= 300,
		"error":   err,
		"data":    data,
	})
}
